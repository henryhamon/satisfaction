package main

import (
	"math"
	"math/rand"
	"strconv"
	"strings"
	"time"

	_ "gopkg.in/mattn/go-sqlite3.v1"
)

// SurveyReports will return the JSON containing all data of the report
type SurveyReports struct {
	Surveys []ReportSurvey
}

// ReportSurvey will hold 1 survey
type ReportSurvey struct {
	SurveyInternalID  int
	SurveyDescription string
	SurveyQuestions   []ReportQuestion
}

// ReportQuestion will hold 1 survey question
type ReportQuestion struct {
	QuestionInternalID      int
	QuestionDescription     string
	QuestionOptions         []string
	QuestionOptionsAnswered []int
	Chart                   SimpleChart
}

// SimpleChart is used by every question
type SimpleChart struct {
	Labels   []string        `json:"labels"`
	Datasets []SimpleDataset `json:"datasets"`
}

// SimpleDataset is used in SimpleChart
type SimpleDataset struct {
	Label           string   `json:"label"`
	Data            []int64  `json:"data"`
	BackgroundColor []string `json:"backgroundColor"`
	Fill            bool     `json:"fill"`
}

func reportParamsDefault(startDate, endDate *string) {
	currentTime := time.Now().UTC()

	/*if strings.TrimSpace(*status) == "" {
		*status = 1
	}*/

	if strings.TrimSpace(*endDate) == "" {
		*endDate = currentTime.Format("2006-01-02")
	} else {
		days, _ := time.Parse("02/01/2006", *endDate)
		*endDate = days.Format("2006-01-02")
	}

	if strings.TrimSpace(*startDate) == "" {
		daysAgo := currentTime.AddDate(0, 0, -30)
		*startDate = daysAgo.Format("2006-01-02")
	} else {
		days, _ := time.Parse("02/01/2006", *startDate)
		*startDate = days.Format("2006-01-02")
	}
}

func reportLastRange() (time.Time, time.Time) {
	sql := "Select MAX(date(updated_at)) as lastupdate "
	sql += " from tickets "
	sql += " WHERE status is not 'waiting'"

	var lastupdate time.Time

	rows, err := Db.Query(sql)
	defer rows.Close()
	checkErr(err)
	for rows.Next() {
		err = rows.Scan(&lastupdate)
	}

	rangestart := lastupdate.AddDate(0, 0, -30)

	return rangestart, lastupdate
}

// GeneralReport will generate report for all surveys and all questions
func GeneralReport(startDate, endDate string) SurveyReports {
	reportParamsDefault(&startDate, &endDate)
	report := SurveyReports{}
	surveys := SurveyList()
	for _, v := range surveys {
		survey := ReportSurvey{}
		survey.SurveyInternalID = v.InternalID
		survey.SurveyDescription = v.Description
		questions := LoadQuestions(v.InternalID)
		for _, q := range questions {
			question := ReportQuestion{}
			question.QuestionInternalID = q.QuestionInternalID
			question.QuestionDescription = q.QuestionDescription
			options, amount := loadAmountOfAnsweredOptions(q.QuestionInternalID, startDate, endDate)
			question.QuestionOptions = options
			question.QuestionOptionsAnswered = amount
			question.Chart = optionChart(q.QuestionInternalID, startDate, endDate)
			survey.SurveyQuestions = append(survey.SurveyQuestions, question)
		}
		report.Surveys = append(report.Surveys, survey)
	}
	return report
}
func loadAmountOfAnsweredOptions(question int, startDate, endDate string) ([]string, []int) {
	descriptions := []string{}
	totals := []int{}
	sql := `SELECT COUNT(o.description), o.description
		FROM answers a
		JOIN options o ON a.option_id = o.internal_id
		WHERE o.question_id = ? AND (date(a.created_at) BETWEEN ? AND ? )
		GROUP BY o.internal_id
		ORDER BY o.position`
	rows, err := Db.Query(sql, question, startDate, endDate)
	defer rows.Close()
	checkErr(err)
	var desc string
	var total int
	for rows.Next() {
		err = rows.Scan(&total, &desc)
		checkErr(err)
		descriptions = append(descriptions, desc)
		totals = append(totals, total)
	}
	return descriptions, totals
}
func optionChart(questionID int, startDate, endDate string) SimpleChart {
	chart := SimpleChart{}
	chart.Labels = getLabels(startDate, endDate)
	chart.Datasets = getDatasets(chart.Labels, questionID)
	return chart
}

func getLabels(startDate, endDate string) []string {
	labels := []string{}
	sql := `SELECT strftime('%d/%m/%Y',created_at) as dtanswer
					FROM answers
					WHERE (date(created_at) BETWEEN ? AND ? )
					GROUP BY date(created_at)
					ORDER BY date(created_at)`

	rows, err := Db.Query(sql, startDate, endDate)
	defer rows.Close()
	checkErr(err)
	var lbl string
	for rows.Next() {
		err = rows.Scan(&lbl)
		checkErr(err)
		labels = append(labels, lbl)
	}
	return labels
}

func getDatasets(dates []string, questionID int) []SimpleDataset {
	datasets := []SimpleDataset{}
	options := LoadOptions(questionID)
	for _, o := range options {
		dataset := &SimpleDataset{}
		dataset.Label = o.OptionDescription
		dataset.Fill = true
		color := rainbow()
		for _, date := range dates {
			dataset.BackgroundColor = append(dataset.BackgroundColor, color)
			dataset.Data = append(dataset.Data, loadAmountByOptionAndDay(o.OptionInternalID, date))
		}
		datasets = append(datasets, *dataset)
	}
	return datasets
}

func loadAmountByOptionAndDay(optionID int, date string) int64 {
	var Amount int64
	sql := `SELECT COUNT(option_id) As AnswerAmount
					FROM answers
					WHERE strftime('%d/%m/%Y',created_at) = ?
					AND option_id = ?`
	rows, err := Db.Query(sql, date, optionID)
	defer rows.Close()
	checkErr(err)
	var total int64
	for rows.Next() {
		err = rows.Scan(&total)
		checkErr(err)
		Amount = total
		break
	}
	return Amount
}

func AppendIfMissing(slice []string, i string) []string {
	for _, ele := range slice {
		if ele == i {
			return slice
		}
	}
	return append(slice, i)
}

func rainbow() string {

	var r, g, b float64
	step := float64(rand.Intn(45) + 1)
	h := step / 50
	i := math.Floor(h * 6)
	f := h*6 - i
	q := 1 - f
	os := math.Remainder(i, 6)

	switch os {
	case 0:
		r = 1
		g = f
		b = 0
	case 1:
		r = q
		g = 1
		b = 0
	case 2:
		r = 0
		g = 1
		b = f
	case 3:
		r = 0
		g = q
		b = 1
	case 4:
		r = f
		g = 0
		b = 1
	case 5:
		r = 1
		g = 0
		b = q
	}
	r = r * 255
	g = g * 255
	b = b * 255

	output := "rgba("
	output += strconv.Itoa(int(r)) + ","
	output += strconv.Itoa(int(g)) + ","
	output += strconv.Itoa(int(b)) + ",1)"

	if output == "rgba(0,0,0,1)" {
		output = rainbow()
	}

	return output
}
