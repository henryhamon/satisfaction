package main

import (
	"gopkg.in/gin-gonic/gin.v1"
)

func showReportPage(c *gin.Context) {
	startDate := c.Query("startdate")
	endDate := c.Query("enddate")

	render(c, gin.H{
		"title":     "Relatório",
		"startDate": startDate,
		"endDate":   endDate,
		"generalReport": GeneralReport(startDate,endDate)}, "report.html")
}
