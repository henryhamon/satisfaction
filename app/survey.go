// Users.go
package main

import (
	"database/sql"
	//"encoding/json"
	"time"

	_ "gopkg.in/mattn/go-sqlite3.v1"
)

// Survey holds the user
type Survey struct {
	InternalID      int
	UserID          string
	Active          string
	Description     string
	Totem           string
	InitialText     string
	FinalText       string
	BackgroundColor string
	FontColor       string
	CreatedAt       string
	UpdatedAt       string
}

// SaveSurvey will save new and existing surveys
func SaveSurvey(internalID int, user string, active int, description, totem, initialtext, finaltext, background, font string) (int, error) {
	var id int64
	if internalID == 0 {
		createdAt := time.Now()
		stmt, err := Db.Prepare("INSERT INTO surveys(user_id,active,description,totem,initial_text,final_text,background_color,font_color,created_at,updated_at) values(?,?,?,?,?,?,?,?,?,?)")
		if err != nil {
			return 0, err
		}
		res, err := stmt.Exec(user, active, description, totem, initialtext, finaltext, background, font, createdAt, createdAt)
		if err != nil {
			return 0, err
		}
		id, err = res.LastInsertId()
		if err != nil {
			return 0, err
		}
	} else {
		updatedAt := time.Now()
		id = int64(internalID)
		stmt, err := Db.Prepare("UPDATE surveys SET user_id = ?, active = ?, description = ?, totem = ?, initial_text = ?, final_text = ?, background_color = ?, font_color = ?, updated_at = ? WHERE internal_id = ?")
		if err != nil {
			return 0, err
		}
		_, err = stmt.Exec(user, active, description, totem, initialtext, finaltext, background, font, updatedAt, internalID)
		if err != nil {
			return 0, err
		}
	}
	return int(id), nil
}

// LoadSurvey will load the survey using internal_id
func LoadSurvey(internalID int) *SurveyJSON {
	rows, err := Db.Query("SELECT internal_id,user_id,description,active,totem,initial_text,final_text,background_color,font_color FROM surveys WHERE internal_id = ?", internalID)
	checkErr(err)
	survey := &SurveyJSON{}
	var (
		user        sql.NullString
		description sql.NullString
		totem       sql.NullString
		initial     sql.NullString
		final       sql.NullString
		background  sql.NullString
		font        sql.NullString
		surveyID    int
		active      int
	)
	for rows.Next() {
		err = rows.Scan(&surveyID, &user, &description, &active, &totem, &initial, &final, &background, &font)
		checkErr(err)
		survey.SurveyInternalID = surveyID
		survey.SurveyActive = active
		if user.Valid {
			survey.SurveyUserID = user.String
		}
		if description.Valid {
			survey.SurveyDescription = description.String
		}
		if totem.Valid {
			survey.SurveyTotem = totem.String
		}
		if initial.Valid {
			survey.SurveyInitialText = initial.String
		}
		if final.Valid {
			survey.SurveyFinalText = final.String
		}
		if background.Valid {
			survey.SurveyBackgroundColor = background.String
		}
		if font.Valid {
			survey.SurveyFontColor = font.String
		}
		break
	}
	rows.Close()
	// Load the questions
	survey.SurveyQuestions = LoadQuestions(survey.SurveyInternalID)
	return survey
}

// SurveyList will return all surveys of the db
func SurveyList() []Survey {
	var surveyList = []Survey{}
	rows, err := Db.Query("SELECT internal_id,description,totem,created_at,active FROM surveys")
	defer rows.Close()
	checkErr(err)

	var (
		description sql.NullString
		totem       sql.NullString
	)
	var internalID int
	var active int
	for rows.Next() {
		s := &Survey{}
		var crat time.Time
		err = rows.Scan(&internalID, &description, &totem, &crat, &active)
		checkErr(err)
		if description.Valid {
			s.Description = description.String
		}
		if totem.Valid {
			s.Totem = totem.String
		}
		if active == 1 {
			s.Active = "true"
		}
		s.InternalID = internalID
		s.CreatedAt = crat.Format("02/01/2006 15:04:05")
		surveyList = append(surveyList, *s)
	}
	return surveyList
}

// DeleteSurvey deletes the survey in database
func DeleteSurvey(surveyID int) error {
	optionsql := `DELETE
		FROM options
		WHERE internal_id IN (
			SELECT o.internal_id FROM surveys s
			JOIN questions q ON s.internal_id = q.survey_id
			JOIN options o ON q.internal_id = o.question_id
			WHERE s.internal_id = ?
		)`
	answerssql := `DELETE
		FROM answers
		WHERE internal_id IN (
			SELECT a.internal_id FROM surveys s
			JOIN questions q ON s.internal_id = q.survey_id
			JOIN answers a ON q.internal_id = a.question_id
			WHERE s.internal_id = ?
			)`
	questionsql := `DELETE
		FROM questions
		WHERE internal_id IN (
			SELECT q.internal_id FROM surveys s
			JOIN questions q ON s.internal_id = q.survey_id
			WHERE s.internal_id = ?
		)`

	surveysql := `DELETE
					FROM surveys
					WHERE internal_id = ?`
	stmt, err := Db.Prepare(optionsql)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(surveyID)
	if err != nil {
		return err
	}
	stmt, err = Db.Prepare(questionsql)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(surveyID)
	if err != nil {
		return err
	}
	stmt, err = Db.Prepare(surveysql)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(surveyID)
	if err != nil {
		return err
	}
	stmt, err = Db.Prepare(answerssql)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(surveyID)
	if err != nil {
		return err
	}
	return nil
}

// GetAvaiableTotems will list the avaiable totens for selection
func GetAvaiableTotems() []string {
	sqlquery := `SELECT DISTINCT totem
		FROM surveys
		WHERE active = ?`
	totems := []string{}
	rows, err := Db.Query(sqlquery, 1)
	defer rows.Close()
	checkErr(err)

	var totem sql.NullString
	for rows.Next() {
		err = rows.Scan(&totem)
		checkErr(err)
		if totem.Valid {
			totems = append(totems, totem.String)
		}
	}
	return totems
}

// GetSurvey will return the id of survey with the name of the totem
func GetSurvey(totem string) int {
	sqlquery := `SELECT internal_id
		FROM surveys
		WHERE totem = ? LIMIT 1`
	rows, err := Db.Query(sqlquery, totem)
	defer rows.Close()
	checkErr(err)

	var id int
	for rows.Next() {
		err = rows.Scan(&id)
		checkErr(err)
	}
	return id
}
