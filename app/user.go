// Users.go
package main

import (
	"database/sql"
	b64 "encoding/base64"
	"errors"
	"fmt"
	"strings"
	"time"

	_ "gopkg.in/mattn/go-sqlite3.v1"
)

// User holds the user
type User struct {
	Name      string
	Email     string
	Username  string
	Pwd       string
	Admin     string
	CreatedAt string
}

func isUserValid(username, password string) bool {
	password = b64.StdEncoding.EncodeToString([]byte(password))
	rows, err := Db.Query("SELECT email as e, username as u FROM users Where (email = ? OR username = ?) AND pwd = ?", username, username, password)
	defer rows.Close()

	if err != nil {
		return false
	}
	var e sql.NullString
	var u sql.NullString
	for rows.Next() {
		rows.Scan(&e, &u)
		var tmpMail string
		var tmpUser string
		if e.Valid {
			tmpMail = e.String
		}
		if u.Valid {
			tmpUser = u.String
		}

		if tmpMail == username || tmpUser == username {
			return true
		}
	}

	return false
}

// SaveNewUser will save the new user
func SaveNewUser(name, email, password, admin, username string) (*User, error) {
	if strings.TrimSpace(password) == "" {
		return nil, errors.New("The password can't be empty")
	}

	pwd := b64.StdEncoding.EncodeToString([]byte(password))
	/*
		  sDec, _ := b64.StdEncoding.DecodeString(sEnc)
			fmt.Println(string(sDec))
	*/
	var adm int
	if admin == "admin" {
		adm = 1
	} else {
		adm = 0
	}
	createdAt := time.Now()
	u := User{Name: name, Email: email, Pwd: pwd, Admin: admin, Username: username, CreatedAt: createdAt.Format("02/01/2006 15:05:05")}

	stmt, err := Db.Prepare("INSERT INTO users(name, email, pwd, admin, created_at,username) values(?,?,?,?,?,?)")
	if err != nil {
		return nil, err
	}
	_, err = stmt.Exec(name, email, pwd, adm, createdAt, username)
	if err != nil {
		return nil, err
	}

	return &u, nil
}

// SaveUser will save the existing user
func SaveUser(name, email, password, admin, username string) error {
	var adm int
	if admin == "admin" {
		adm = 1
	} else {
		adm = 0
	}

	if strings.TrimSpace(password) == "" { // empty password will not update the existing password
		stmt, err := Db.Prepare("UPDATE users SET name = ?, admin = ?, email = ? WHERE username = ?")
		if err != nil {
			return err
		}
		_, err = stmt.Exec(name, adm, email, username)
		if err != nil {
			return err
		}
	} else { // if password is not empty it will update the existing
		pwd := b64.StdEncoding.EncodeToString([]byte(password))
		stmt, err := Db.Prepare("UPDATE users SET name = ?, admin = ?, pwd = ?, email = ? WHERE username = ?")
		if err != nil {
			return err
		}
		_, err = stmt.Exec(name, adm, pwd, email, username)
		if err != nil {
			return err
		}
	}
	return nil
}

// LoadUser will load the user using username
func LoadUser(username string) *User {
	rows, err := Db.Query("SELECT username,name, email,pwd,admin FROM users WHERE username = ? OR email = ?", username, username)
	defer rows.Close()
	checkErr(err)
	user := &User{}
	var (
		uname sql.NullString
		name  sql.NullString
		email sql.NullString
		pwd   sql.NullString
	)
	var adm int
	for rows.Next() {
		err = rows.Scan(&uname, &name, &email, &pwd, &adm)
		checkErr(err)
		if pwd.Valid {
			tmpPwd, err := b64.StdEncoding.DecodeString(pwd.String)
			checkErr(err)
			user.Pwd = fmt.Sprintf("%s", tmpPwd)
		}
		if uname.Valid {
			user.Username = uname.String
		}
		if name.Valid {
			user.Name = name.String
		}
		if email.Valid {
			user.Email = email.String
		}
		if adm == 1 {
			user.Admin = "admin"
		}
		break
	}
	return user
}

// UserList will return all users of the db
func UserList() []User {
	var userList = []User{}
	rows, err := Db.Query("SELECT name, email,pwd,admin,created_at,username FROM users")
	defer rows.Close()
	checkErr(err)
	u := &User{}
	var (
		name     sql.NullString
		email    sql.NullString
		pwd      sql.NullString
		username sql.NullString
	)
	var adm int
	for rows.Next() {
		var crat time.Time
		err = rows.Scan(&name, &email, &pwd, &adm, &crat, &username)
		checkErr(err)
		if email.Valid {
			u.Email = email.String
		}
		if name.Valid {
			u.Name = name.String
		}
		if pwd.Valid {
			u.Pwd = pwd.String
		}
		if adm == 1 {
			u.Admin = "admin"
		}
		if username.Valid {
			u.Username = username.String
		}
		u.CreatedAt = crat.Format("02/01/2006 15:04:05")
		userList = append(userList, *u)
	}
	return userList
}

// DeleteUser deletes the user in database
func DeleteUser(username string) error {
	stmt, err := Db.Prepare("DELETE FROM users WHERE username = ?")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(username)
	if err != nil {
		return err
	}
	return nil
}
