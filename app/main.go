package main

import (
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/jinzhu/configor"
	"gopkg.in/gin-gonic/gin.v1"
	_ "gopkg.in/mattn/go-sqlite3.v1"
	"gopkg.in/olahol/melody.v1"
)

// ProductName is the name of this product, this will be used to licensing
const ProductName = "Pesquisa de Satisfação"

// DecryptationKey will be used to check license
const DecryptationKey = "bplus_golang_!@#"

// Version is the actual version of the application
const Version = 1.0

// Config is loaded from configuration file
var Config = struct {
	Development struct {
		Adapter  string `default:"sqlite3"`
		Database string `default:"db/database.db?cache=shared&mode=rwc&_busy_timeout=5000"`
	}

	Production struct {
		Adapter  string `default:"sqlite3"`
		Database string `default:"db/database.db?cache=shared&mode=rwc&_busy_timeout=5000"`
	}

	Test struct {
		Adapter  string `default:"sqlite3"`
		Database string `default:"db/database.db?cache=shared&mode=rwc&_busy_timeout=5000"`
	}

	Settings struct {
		Port string `default:"5000"`
	}
}{}

var (
	router *gin.Engine
	// Db holds the database opened to use in package for executing queries
	Db *sql.DB
	// M has the instance of melody
	M *melody.Melody
	//Env configures the actual environment
	Env string
)

func init() {
	Env = os.Getenv("ENV")

	flag.StringVar(&Env, "e", Env, "Environment")
}

func main() {
	flag.Parse()
	configor.Load(&Config, "config.yml")
	//Check if the app is licensed
	checkLicense()

	//gin.SetMode(gin.ReleaseMode)
	Db = initializeDB("")
	defer Db.Close()

	GinEngine(gin.ReleaseMode)
	//router = gin.Default()
	//router.Static("/public", "./public")
	//router.LoadHTMLGlob("templates/*")

	M = melody.New()

	//initializeRoutes()

	//Melody receives here the messages passed by ws.Send (in javascript)
	M.HandleMessage(func(s *melody.Session, msg []byte) {
		var strMessage string
		strMessage = string(msg)
		switch s.Request.URL.String() {
		//case "/ticket/create/normal":
		default:
			fmt.Println("URL => ", s.Request.URL.String(), "      NEW MESSAGE => ", strMessage)
		}

		M.Broadcast(msg)
	})

	router.Run(":" + Config.Settings.Port)

}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func render(c *gin.Context, data gin.H, templateName string) {
	/*
		loggedInInterface, _ := c.Get("is_logged_in")
		data["is_logged_in"] = loggedInInterface.(bool)
	*/

	switch c.Request.Header.Get("Accept") {
	case "application/json":
		// Respond with JSON
		c.JSON(http.StatusOK, data["payload"])
	case "application/xml":
		// Respond with XML
		c.XML(http.StatusOK, data["payload"])
	default:
		// Respond with HTML
		user := getActivedUserData(c)
		data["role"] = getUserRole(c)
		data["version"] = fmt.Sprintf("%.1f", Version)
		data["licensedTo"] = LicenseFile.Company
		data["user"] = user
		c.HTML(http.StatusOK, templateName, data)
	}
}

func initializeDB(testingDbName string) *sql.DB {
	var databaseName string
	if strings.ToLower(Env) == "test" {
		databaseName = Config.Test.Database
	} else if strings.ToLower(Env) == "dev" {
		databaseName = Config.Development.Database
	} else {
		databaseName = Config.Development.Database
		//databaseName = Config.Production.Database
	}

	if testingDbName != "" {
		databaseName = testingDbName
	}

	db, err := sql.Open("sqlite3", databaseName)
	checkErr(err)
	db.SetMaxOpenConns(1)
	return db
}
