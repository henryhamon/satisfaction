package main

import (
	//"net/http"
	b64 "encoding/base64"
	"math/rand"
	"net/http"
	"strconv"
	"strings"

	"gopkg.in/gin-gonic/gin.v1"
)

func showLoginPage(c *gin.Context) {
	render(c, gin.H{"title": "Login"}, "login.html")
}

func showUserHomePage(c *gin.Context) {
	postLogin := c.Query("login")
	if postLogin == "true" {
		err, days := checkLicense()
		if days <= 3 && !err {
			render(c, gin.H{"title": "Home", "role": getUserRole(c), "expirationIn": days}, "userhome.html")
		} else {
			render(c, gin.H{"title": "Home", "role": getUserRole(c)}, "userhome.html")
		}
	} else {
		render(c, gin.H{"title": "Home", "role": getUserRole(c)}, "userhome.html")
	}
}
func showAdminPage(c *gin.Context) {
	render(c, gin.H{"title": "Administração", "role": getUserRole(c)}, "admin.html")
}

func performLogin(c *gin.Context) {

	username := c.PostForm("username")
	password := c.PostForm("password")

	// Check if the username/password combination is valid
	if isUserValid(username, password) {
		// If the username/password is valid set the token in a cookie
		token := generateSessionToken()
		c.SetCookie("token", token, 28800, "", "", false, true)
		user := LoadUser(username)
		c.SetCookie("user_name", b64.StdEncoding.EncodeToString([]byte(user.Name)), 28800, "", "", false, true)
		c.SetCookie("user_username", b64.StdEncoding.EncodeToString([]byte(user.Username)), 28800, "", "", false, true)
		c.SetCookie("user_email", b64.StdEncoding.EncodeToString([]byte(user.Email)), 28800, "", "", false, true)
		c.SetCookie("user_admin", b64.StdEncoding.EncodeToString([]byte(user.Admin)), 28800, "", "", false, true)
		c.Set("is_logged_in", true)
		c.Redirect(http.StatusMovedPermanently, "/u/home?login=true")
		//render(c, gin.H{"title": "Home", "role": user.Role}, "userhome.html")
		//render(c, gin.H{"title": "Usuários"}, "user.html")
	} else {
		// If the username/password combination is invalid,
		// show the error message on the login page
		c.HTML(http.StatusBadRequest, "login.html", gin.H{
			"ErrorTitle":   "Falha no Login",
			"ErrorMessage": "Credenciais inválidas."})
	}
}

func generateSessionToken() string {
	// We're using a random 16 character string as the session token
	// This is NOT a secure way of generating session tokens
	// DO NOT USE THIS IN PRODUCTION
	return strconv.FormatInt(rand.Int63(), 16)
}

func logout(c *gin.Context) {
	// Clear the cookie
	c.SetCookie("token", "", -1, "", "", false, true)
	c.SetCookie("user_admin", "", -1, "", "", false, true)
	c.SetCookie("user_name", "", -1, "", "", false, true)
	c.SetCookie("user_username", "", -1, "", "", false, true)
	c.SetCookie("user_email", "", -1, "", "", false, true)
	// Redirect to the home page
	c.Redirect(http.StatusTemporaryRedirect, "/u/login")
}

/*
func showRegistrationPage(c *gin.Context) {
	// Call the render function with the name of the template to render
	render(c, gin.H{
		"title": "Register"}, "register.html")
}

func register(c *gin.Context) {
	// Obtain the POSTed username and password values
	username := c.PostForm("username")
	password := c.PostForm("password")

	if _, err := registerNewUser(username, password); err == nil {
		// If the user is created, set the token in a cookie and log the user in
		token := generateSessionToken()
		c.SetCookie("token", token, 3600, "", "", false, true)
		c.Set("is_logged_in", true)

		render(c, gin.H{
			"title": "Successful registration & Login"}, "login-successful.html")

	} else {
		// If the username/password combination is invalid,
		// show the error message on the login page
		c.HTML(http.StatusBadRequest, "register.html", gin.H{
			"ErrorTitle":   "Registration Failed",
			"ErrorMessage": err.Error()})

	}
}
*/
func createNewUser(c *gin.Context) {
	payload := make(map[string]string)
	payload["type"] = "crud"
	payload["isNew"] = "true"
	render(c, gin.H{
		"title":   "Novo Usuário",
		"role":    getUserRole(c),
		"payload": payload}, "user.html")
}

func saveUser(c *gin.Context) {
	errString := ""
	title := "Novo Usuário"
	name := c.PostForm("userName")
	username := c.PostForm("userUsername")
	email := c.PostForm("userEmail")
	pwd := c.PostForm("userPassword")
	admin := c.PostForm("userAdmin")
	newUser := c.PostForm("isNew")
	if newUser == "true" {
		_, err := SaveNewUser(name, email, pwd, admin, username)
		if err != nil {
			if err.Error() == "The password can't be empty" {
				errString = "A senha não pode estar vazia!"
			} else {
				errString = err.Error()
			}
		}
	} else { //already exists
		title = "Editar Usuário"
		err := SaveUser(name, email, pwd, admin, username)
		if err != nil {
			errString = err.Error()
		}
	}

	if errString == "" {
		/*render(c, gin.H{"title": "Usuários", "role": getUserRole(c)}, "user.html")
		c.AbortWithStatus(http.StatusOK)*/
		c.Redirect(http.StatusMovedPermanently, "/u/admin/users")
	} else { //Some error ocurred
		payload := make(map[string]string)
		payload["type"] = "crud"
		payload["userName"] = name
		payload["userEmail"] = email
		payload["userUsername"] = username
		payload["userPassword"] = pwd
		payload["userAdmin"] = admin
		payload["isNew"] = newUser
		s := []string{"Não foi possível salvar, verifique erros no formulário! Detalhes: ", errString}
		render(c, gin.H{
			"title":        title,
			"payload":      payload,
			"role":         getUserRole(c),
			"ErrorTitle":   "Problema ao salvar!",
			"ErrorMessage": strings.Join(s, "")}, "user.html")
	}
}

func editUser(c *gin.Context) {
	id := c.Param("username")
	payload := make(map[string]string)
	payload["type"] = "crud"
	payload["isNew"] = "false"
	user := LoadUser(id)
	payload["userName"] = user.Name
	payload["userEmail"] = user.Email
	payload["userUsername"] = user.Username
	payload["userPassword"] = user.Pwd
	payload["userAdmin"] = user.Admin
	render(c, gin.H{
		"title":   "Editar Usuário",
		"role":    getUserRole(c),
		"payload": payload}, "user.html")
}

func deleteUser(c *gin.Context) {
	err := c.Request.ParseForm()
	checkErr(err)
	username := c.Query("userUsername")
	if err == nil {
		ret := DeleteUser(username)
		if ret == nil {
			c.JSON(http.StatusOK, gin.H{})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{})
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{})
	}
}

func userList(c *gin.Context) {
	users := UserList()
	payload := make(map[string]string)
	payload["type"] = "list"
	render(c, gin.H{
		"title":   "Usuários",
		"payload": payload,
		"role":    getUserRole(c),
		"users":   users}, "user.html")
}

func getUserRole(c *gin.Context) string {
	return decodeCookie("user_admin", c)
}
func getActivedUserData(c *gin.Context) *User {
	user := &User{}
	user.Admin = decodeCookie("user_admin", c)
	user.Name = decodeCookie("user_name", c)
	user.Email = decodeCookie("user_email", c)
	user.Username = decodeCookie("user_username", c)
	return user
}
func decodeCookie(name string, c *gin.Context) string {
	cookie, err := c.Cookie(name)
	if err == nil {
		byteArray, err := b64.StdEncoding.DecodeString(cookie)
		checkErr(err)
		return string(byteArray[:])
	}
	return ""
}
