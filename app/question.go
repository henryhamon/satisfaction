// Question.go
package main

import (
	"database/sql"
	"encoding/json"
	"time"

	_ "gopkg.in/mattn/go-sqlite3.v1"
)

/*
type Question struct {
	InternalID      int			`gorm:"primary_key;AUTO_INCREMENT";"not null"`
	Description     string	`gorm:"not null"`
	Tips            string
	Position        int    `gorm:"not null"`
  Options         []Option
}
*/

// SaveQuestion will save new and existing questions
func SaveQuestion(internalID int, surveyID int, description string, tips string, position int) (int, error) {
	var id int64
	if internalID == 0 {
		createdAt := time.Now()
		stmt, err := Db.Prepare("INSERT INTO questions (survey_id,description,tips,position,created_at,updated_at) VALUES (?,?,?,?,?,?)")
		if err != nil {
			return 0, err
		}
		res, err := stmt.Exec(surveyID, description, tips, position, createdAt, createdAt)
		if err != nil {
			return 0, err
		}
		id, err = res.LastInsertId()
		if err != nil {
			return 0, err
		}
	} else {
		updatedAt := time.Now()
		id = int64(internalID)
		stmt, err := Db.Prepare("UPDATE questions SET survey_id = ?,description = ?,tips = ?,position = ?,updated_at = ? WHERE internal_id = ?")
		if err != nil {
			return 0, err
		}
		_, err = stmt.Exec(surveyID, description, tips, position, updatedAt, internalID)
		if err != nil {
			return 0, err
		}
	}
	return int(id), nil
}

// LoadQuestions will load the questions of the survey
func LoadQuestions(surveyID int) []QuestionJSON {
	var questionList = []QuestionJSON{}

	rows, err := Db.Query("SELECT internal_id,description,tips FROM questions WHERE survey_id = ? ORDER BY position ASC", surveyID)
	checkErr(err)
	var (
		questionID  int
		description sql.NullString
		tips        sql.NullString
	)
	for rows.Next() {
		question := &QuestionJSON{}
		err = rows.Scan(&questionID, &description, &tips)
		checkErr(err)
		question.QuestionInternalID = questionID
		if description.Valid {
			question.QuestionDescription = description.String
		}
		if tips.Valid {
			question.QuestionTip = tips.String
		}
		questionList = append(questionList, *question)
	}
	rows.Close()
	// Load the options
	for k := range questionList {
		questionList[k].QuestionOptions = LoadOptions(questionList[k].QuestionInternalID)
		a, _ := json.Marshal(questionList[k])
		str := string(a)
		questionList[k].QuestionString = str
	}
	return questionList
}

// DeleteQuestion deletes the question in database
func DeleteQuestion(questionID int) error {
	optionsql := `DELETE
		FROM options
		WHERE internal_id IN (
			SELECT o.internal_id
			FROM options o
			JOIN questions q
			ON q.internal_id = o.question_id
			WHERE q.internal_id = ?
		)`

	questionsql := `DELETE FROM questions WHERE internal_id = ?`
	answerssql := `DELETE FROM answers WHERE question_id = ?`
	stmt, err := Db.Prepare(optionsql)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(questionID)
	if err != nil {
		return err
	}
	stmt, err = Db.Prepare(questionsql)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(questionID)
	if err != nil {
		return err
	}
	stmt, err = Db.Prepare(answerssql)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(questionID)
	if err != nil {
		return err
	}
	return nil
}
