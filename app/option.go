// Option.go
package main

import (
	"database/sql"
	//"encoding/json"
	"time"

	_ "gopkg.in/mattn/go-sqlite3.v1"
)

/*
type Option struct {
	InternalID      int			`gorm:"primary_key;AUTO_INCREMENT";"not null"`
	Description     string	`gorm:"not null"`
	Position        int    `gorm:"not null"`
	BackgroundColor string	`sql:"DEFAULT:'#fff'"`
	FontColor       string	`sql:"DEFAULT:'#333'"`
}
*/

// SaveOption will save new and existing options
func SaveOption(internalID int, questionID int, description string, position int, background string, font string) (int, error) {
	var id int64
	if internalID == 0 {
		createdAt := time.Now()
		stmt, err := Db.Prepare("INSERT INTO options (question_id,description,position,background_color,font_color,created_at,updated_at) VALUES (?,?,?,?,?,?,?)")
		if err != nil {
			return 0, err
		}
		res, err := stmt.Exec(questionID, description, position, background, font, createdAt, createdAt)
		if err != nil {
			return 0, err
		}
		id, err = res.LastInsertId()
		if err != nil {
			return 0, err
		}
	} else {
		updatedAt := time.Now()
		id = int64(internalID)
		stmt, err := Db.Prepare("UPDATE options SET question_id = ?,description = ?,position = ?,background_color = ?,font_color = ?,updated_at = ? WHERE internal_id = ?")
		if err != nil {
			return 0, err
		}
		_, err = stmt.Exec(questionID, description, position, background, font, updatedAt, internalID)
		if err != nil {
			return 0, err
		}
	}
	return int(id), nil
}

// LoadOptions will load the questions of the survey
func LoadOptions(questionID int) []OptionJSON {
	var optionList = []OptionJSON{}

	rows, err := Db.Query("SELECT internal_id,description,background_color,font_color FROM options WHERE question_id = ? ORDER BY position ASC", questionID)
	checkErr(err)
	var (
		optionID    int
		description sql.NullString
		background  sql.NullString
		font        sql.NullString
	)
	for rows.Next() {
		option := &OptionJSON{}
		err = rows.Scan(&optionID, &description, &background, &font)
		checkErr(err)
		option.OptionInternalID = optionID
		if description.Valid {
			option.OptionDescription = description.String
		}
		if background.Valid {
			option.OptionBackgroundColor = background.String
		}
		if font.Valid {
			option.OptionFontColor = font.String
		}
		optionList = append(optionList, *option)
	}
	rows.Close()
	return optionList
}

// DeleteOption deletes the option in database
func DeleteOption(optionID int) error {
	sql := `DELETE
					FROM options
					WHERE internal_id = ?`
	stmt, err := Db.Prepare(sql)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(optionID)
	if err != nil {
		return err
	}
	sqlAnswers := `DELETE
					FROM answers
					WHERE option_id = ?`
	stmt, err = Db.Prepare(sqlAnswers)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(optionID)
	if err != nil {
		return err
	}
	return nil
}
