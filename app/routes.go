// routes.go
package main

import (
	"net/http"

	"gopkg.in/gin-gonic/gin.v1"
)

// GinEngine will initialize the routes
func GinEngine(relMode string) *gin.Engine {
	gin.SetMode(relMode)
	router = gin.Default()
	router.Static("/public", "./public")
	router.LoadHTMLGlob("templates/*")
	initializeRoutes()

	return router

}
func initializeRoutes() {
	// middleware
	router.Use(setUserStatus())

	router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/u/login")
		//http.ServeFile(c.Writer, c.Request, "index.html")
	})
	totemRoutes := router.Group("/totem")
	{
		totemRoutes.GET("/", showTotemMainPage)
		totemRoutes.POST("/save", saveAnswers)
	}

	loggedUserRoutes := router.Group("/u")
	{
		// Routes avaiable only to users with 'role' = "admin"
		adminRoutes := loggedUserRoutes.Group("/admin")
		{
			adminRoutes.GET("/", beforeMiddleware(), ensureLoggedIn(), showAdminPage)
			// User routes
			adminRoutes.GET("/users", beforeMiddleware(), ensureLoggedIn(), ensureIsAdmin(), userList) // List
			userRoutes := adminRoutes.Group("/user")
			{
				userRoutes.GET("/new", beforeMiddleware(), ensureLoggedIn(), ensureIsAdmin(), createNewUser)       // Show
				userRoutes.POST("/new", beforeMiddleware(), ensureLoggedIn(), ensureIsAdmin(), saveUser)           // Create and Edit
				userRoutes.GET("/edit/:username", beforeMiddleware(), ensureLoggedIn(), ensureIsAdmin(), editUser) // Show Edit
				userRoutes.DELETE("/", beforeMiddleware(), ensureLoggedIn(), ensureIsAdmin(), deleteUser)          // Delete
			}
		}
		// Survey routes
		loggedUserRoutes.GET("/surveys", beforeMiddleware(), ensureLoggedIn(), surveyList) // List
		surveyRoutes := loggedUserRoutes.Group("/survey")
		{
			surveyRoutes.GET("/new", beforeMiddleware(), ensureLoggedIn(), createNewSurvey)        // Show
			surveyRoutes.POST("/save", beforeMiddleware(), ensureLoggedIn(), saveSurvey)           // Save
			surveyRoutes.GET("/edit/:id", beforeMiddleware(), ensureLoggedIn(), editSurvey)        // Show Edit
			surveyRoutes.DELETE("/", beforeMiddleware(), ensureLoggedIn(), deleteSurvey)           // Delete
			surveyRoutes.DELETE("/question", beforeMiddleware(), ensureLoggedIn(), deleteQuestion) // Delete
			surveyRoutes.DELETE("/option", beforeMiddleware(), ensureLoggedIn(), deleteOption)     // Delete
		}

		// Report routes
		loggedUserRoutes.GET("/report", beforeMiddleware(), ensureLoggedIn(), showReportPage) // List

		loggedUserRoutes.GET("/login", beforeMiddleware(), ensureNotLoggedIn(), showLoginPage)
		loggedUserRoutes.POST("/login", beforeMiddleware(), ensureNotLoggedIn(), performLogin)
		loggedUserRoutes.GET("/logout", beforeMiddleware(), ensureLoggedIn(), logout)
		loggedUserRoutes.GET("/home", beforeMiddleware(), ensureLoggedIn(), showUserHomePage)
	}
}
