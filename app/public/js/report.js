function rajax(type, url, data, functionOnSuccess, functionOnError){
  $.ajax({
    method: type,
    url: url,
    data: data
  }).done(function( msg ) {
    functionOnSuccess(msg);
  }).fail(function(msg) {
    functionOnError(msg);
  });
};
function update_all(req){
  rajax("POST", "/u/report", req, function(resp) {

    barChart.data = resp.barDataset;
    pieChart.data = resp.pieDataset;
    lnChart.data = resp.lineDataset;
    ln2Chart.data = resp.lineWindowDataset;
    radarChart.data = resp.radarDataset;

    pieChart.update();
    lnChart.update();
    ln2Chart.update();
    barChart.update();
    radarChart.update();
  }, function(err){
    console.log(err);
  });
};

Date.prototype.ymd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
        ].join('-');
};

Date.prototype.dmy = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();
  return [(dd>9 ? '' : '0') + dd,
          (mm>9 ? '' : '0') + mm,
          this.getFullYear()
        ].join('/');
};
//<!--
var today = new Date()
var initialDate = new Date().setDate(today.getDate()-30);
//var initialDate = new Date(2017, 4, 21);
var finalDate =  Date.now();
if(getUrlVars()["startdate"]!=undefined){
  var date = getUrlVars()["startdate"].split('/');
  var y = parseInt(date[2]);
  var m = parseInt(date[1])-1;
  var d = parseInt(date[0]);
  initialDate = new Date(y,m,d);
}
if(getUrlVars()["enddate"]!=undefined){
  var date = getUrlVars()["enddate"].split('/');
  var y = parseInt(date[2]);
  var m = parseInt(date[1])-1;
  var d = parseInt(date[0]);
  finalDate = new Date(y,m,d);
}
$("#slider").dateRangeSlider(
  {
    symmetricPositionning: false,
    step:{
      days: 1
    },
    range:{
      min: {days: 2},
      max: {days: 80}
    },
    bounds:{
      min: new Date(2017, 0, 1),
      max: new Date().setDate(today.getDate()+7)
    },
    defaultValues:{
      min: initialDate,
      max: finalDate
    },
    formatter:function(val){
      return val.dmy();
    }
  }
).bind("valuesChanged", function(e, data){
  var startdate = data.values.min.dmy();
  var enddate = data.values.max.dmy();
  location.href = location.href.split("?")[0] + '?startdate='+startdate+'&enddate='+enddate;
  /*req = {"startdate": startdate , "enddate": enddate};
  update_all(req);*/
});
// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}