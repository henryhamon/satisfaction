package main

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	Key := []byte("bplus_golang_!@#")
	ProductName := "Pesquisa de Satisfação"

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter the 'Company' thats will use the app: ")
	companyName, _ := reader.ReadString('\n')
	companyName = strings.TrimSpace(companyName)
	fmt.Print("Enter the 'Expiration Date' of this license (YYYY-MM-DD): ")
	expirationDate, _ := reader.ReadString('\n')
	expirationDate = strings.TrimSpace(expirationDate)
	fmt.Print("Enter the 'Version' that's this license will work on (ALL|1.0,1.1 ...): ")
	version, _ := reader.ReadString('\n')
	version = strings.TrimSpace(version)

	// encrypt value to base64
	cryptoHash := encrypt(Key, companyName+"|"+expirationDate+"|"+version+"|"+ProductName)

	f, err := os.Create(companyName + "_" + expirationDate + "_" + version + "_license.yml")
	if err != nil {
		panic("Could not create the file!")
	}
	_, err = f.WriteString("company: " + companyName + "\nexpiration: " + expirationDate + "\nversion: " + version + "\nlicense: " + cryptoHash + "\nproductname: " + ProductName)
	if err != nil {
		panic("Was not possible to write the file!")
	}
	f.Sync()

	fmt.Println("File generated with success!")
	// encrypt base64 crypto to original value
	//text := decrypt(key, cryptoText)
	//fmt.Printf(text)
}

// encrypt string to base64 crypto using AES
func encrypt(key []byte, text string) string {
	// key := []byte(keyText)
	plaintext := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// convert to base64
	return base64.URLEncoding.EncodeToString(ciphertext)
}

// decrypt from base64 to decrypted string
func decrypt(key []byte, cryptoText string) string {
	ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext)
}
