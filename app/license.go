package main

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/configor"
)

// LicenseFile is loaded from license file
var LicenseFile = struct {
	Company    string `default:"none"`
	Expiration string
	Version    string
	ProductName string
	License    string
}{}

func checkLicense() (bool, int) {
	hasError := false
	var NumberOfDays int
	// Check if the file 'license.yml' exists
	if licenseExists() {
		// Check if the hash is correct and decrypt it
		isHashOk, decryptedLicense := checkLicenseHash()
		if isHashOk {
			// Check if the hash data is equal the informational data, if it's not it's was changed by someone
			if checkLicenseChanges(decryptedLicense) {
				// Will check if the license can run this version of application
				if checkLicenseVersion() {
					// Will check the expiration date and return the days
					allOk, days := checkLicenseExpiration()
					NumberOfDays = days
					if !allOk {
						hasError = true
					}
				} else {
					hasError = true
				}
			} else {
				hasError = true
			}
		} else {
			hasError = true
		}
	} else {
		hasError = true
	}

	if !hasError {
		fmt.Println("Licenciado para: ", LicenseFile.Company, " - Data de Expiração: ", LicenseFile.Expiration, "(", NumberOfDays, "dias ) - Versão: ", LicenseFile.Version)
		if NumberOfDays <= 5 {
			fmt.Println("ATENÇÃO! Faltam ", NumberOfDays, " dias para a expiração da licença!\nPor favor contate a BPlus Tecnologia para solicitar uma nova licença!\nhttp://www.bplus.com.br - +55(47)3626-2095")
		}
	} else {
		defer os.Exit(-1)
	}

	return hasError, NumberOfDays
}

func checkLicenseExpiration() (bool, int) {
	ok := true
	ExpirationDate, err := time.Parse("2006-01-02", LicenseFile.Expiration)
	checkErr(err)
	days := int(time.Now().Sub(ExpirationDate).Hours() / 24.0 * -1)
	if days < 0 {
		ok = false
		fmt.Println("A LICENÇA ESTÁ VENCIDA!\nPor favor contate a BPlus Tecnologia para solicitar uma nova licença!\nhttp://www.bplus.com.br - +55(47)3626-2095\n\n...")
	}
	return ok, days
}

func checkLicenseVersion() bool {
	ok := true
	if LicenseFile.Version != "ALL" {
		fVersion, err := strconv.ParseFloat(LicenseFile.Version, 64)
		checkErr(err)
		if fVersion < Version {
			ok = false
			fmt.Println("A LICENÇA NÃO COBRE A VERSÃO ATUAL!\nPor favor contate a BPlus Tecnologia para solicitar uma nova licença!\nhttp://www.bplus.com.br - +55(47)3626-2095\n\n...")
		}
	}
	return ok
}

func checkLicenseChanges(decryptedLicense string) bool {
	ok := true
	license := strings.Split(decryptedLicense, "|")
	decCompany := license[0]
	decExpiration := license[1]
	decVersion := license[2]
	decProd := license[3]
	if (decCompany != LicenseFile.Company) || (decExpiration != LicenseFile.Expiration) || (decVersion != LicenseFile.Version) || (decProd != LicenseFile.ProductName) {
		ok = false
		fmt.Println("LICENÇA CORROMPIDA!\nPor favor contate a BPlus Tecnologia para solicitar uma nova licença!\nhttp://www.bplus.com.br - +55(47)3626-2095\n\n...")
	}
	return ok
}

func checkLicenseHash() (bool, string) {
	ok := true
	decryptedLicense := decryptLicense([]byte(DecryptationKey), LicenseFile.License)
	rgx, _ := regexp.Compile("\\|")
	if !rgx.Match([]byte(decryptedLicense)) {
		ok = false
		fmt.Println("LICENÇA CORROMPIDA!\nPor favor contate a BPlus Tecnologia para solicitar uma nova licença!\nhttp://www.bplus.com.br - +55(47)3626-2095\n\n...")
	}
	return ok, decryptedLicense
}

func licenseExists() bool {
	exists := false
	configor.Load(&LicenseFile, "license.yml")
	if LicenseFile.Company == "none" {
		fmt.Println("NÃO LICENCIADO!\nPor favor contate a BPlus Tecnologia para solicitar uma nova licença!\nhttp://www.bplus.com.br - +55(47)3626-2095\n\n...")
	} else {
		exists = true
	}
	return exists
}

// decrypt from base64 to decrypted string
func decryptLicense(key []byte, cryptoText string) string {
	ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext)
}
