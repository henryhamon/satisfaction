// middleware.auth.go
package main

import (
	b64 "encoding/base64"
	"net/http"

	"gopkg.in/gin-gonic/gin.v1"
)

func setUserStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		token, err := c.Cookie("token")
		if err == nil || token != "" {
			c.Set("is_logged_in", true)
		} else {
			c.Set("is_logged_in", false)
		}
		cookie, _ := c.Cookie("user_admin")
		byteArray, _ := b64.StdEncoding.DecodeString(cookie)
		role := string(byteArray[:])
		if role == "admin" {
			c.Set("admin", 1)
		} else {
			c.Set("admin", 0)
		}
	}
}
func beforeMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Request.Header.Set("Pragma", "no-cache")
		c.Request.Header.Set("Cache-Control", "no-cache")
		c.Writer.Header().Set("Pragma", "no-cache")
		c.Writer.Header().Set("Cache-Control", "no-cache")
		c.Next()
	}
}
func ensureNotLoggedIn() gin.HandlerFunc {
	return func(c *gin.Context) {
		loggedInInterface, _ := c.Get("is_logged_in")
		loggedIn := loggedInInterface.(bool)
		if loggedIn {
			c.Redirect(http.StatusMovedPermanently, "/u/home")
			render(c, gin.H{"title": "Home", "role": getUserRole(c), "ErrorTitle": "Usuário já está Logado", "ErrorMessage": "Você já tem uma sessão ativa com o usuário!"}, "userhome.html")
			c.AbortWithStatus(http.StatusUnauthorized)
		}
		c.Next()
	}
}

func ensureLoggedIn() gin.HandlerFunc {
	return func(c *gin.Context) {
		loggedInInterface, _ := c.Get("is_logged_in")
		loggedIn := loggedInInterface.(bool)

		if !loggedIn {
			//showLoginPage(c)
			c.Redirect(http.StatusMovedPermanently, "/u/login")
			c.AbortWithStatus(http.StatusUnauthorized)
		}
		c.Next()
	}
}
func ensureIsAdmin() gin.HandlerFunc {
	return func(c *gin.Context) {
		roleInterface, _ := c.Get("admin")
		role := roleInterface.(int)
		if role < 1 {
			render(c, gin.H{"title": "Home", "role": getUserRole(c), "ErrorTitle": "Problema de Permissão", "ErrorMessage": "Você não tem as permissões necessárias para acessar a página!"}, "userhome.html")
			c.AbortWithStatus(http.StatusUnauthorized)
		}
		c.Next()
	}
}
