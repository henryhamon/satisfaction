package main

import (
	//"net/http"

	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gopkg.in/gin-gonic/gin.v1"
)

// OptionJSON holds the json data of options
type OptionJSON struct {
	OptionInternalID      int    `json:"optionInternalId"`
	OptionDescription     string `json:"optionDescription"`
	OptionBackgroundColor string `json:"optionBackgroundColor"`
	OptionFontColor       string `json:"optionFontColor"`
}

// QuestionJSON holds the json data of questions
type QuestionJSON struct {
	QuestionInternalID  int          `json:"questionInternalId"`
	QuestionDescription string       `json:"questionDescription"`
	QuestionTip         string       `json:"questionTip"`
	QuestionString      string       `json:"questionString"`
	QuestionOptions     []OptionJSON `json:"questionOptions"`
}

// SurveyJSON holds the json data of surveys
type SurveyJSON struct {
	SurveyInternalID      int            `json:"surveyInternalId"`
	SurveyUserID          string         `json:"surveyUserId"`
	SurveyDescription     string         `json:"surveyDescription"`
	SurveyTotem           string         `json:"surveyTotem"`
	SurveyInitialText     string         `json:"surveyInitialText"`
	SurveyFinalText       string         `json:"surveyFinalText"`
	SurveyBackgroundColor string         `json:"surveyBackgroundColor"`
	SurveyFontColor       string         `json:"surveyFontColor"`
	SurveyActive          int            `json:"surveyActive"`
	SurveyQuestions       []QuestionJSON `json:"surveyQuestions"`
}

func createNewSurvey(c *gin.Context) {
	payload := make(map[string]string)
	payload["type"] = "crud"
	payload["surveyBackgroundColor"] = "#f7f7f7"
	payload["surveyFontColor"] = "#202020"
	render(c, gin.H{
		"title":   "Nova Pesquisa",
		"role":    getUserRole(c),
		"payload": payload}, "survey.html")
}

func saveSurvey(c *gin.Context) {
	var request SurveyJSON
	err := json.NewDecoder(c.Request.Body).Decode(&request)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{})
	}
	// First save the survey
	surveyID, err := SaveSurvey(request.SurveyInternalID,
		request.SurveyUserID,
		request.SurveyActive,
		request.SurveyDescription,
		request.SurveyTotem,
		request.SurveyInitialText,
		request.SurveyFinalText,
		request.SurveyBackgroundColor,
		request.SurveyFontColor)
	if err != nil {
		fmt.Println("Cannot save the survey! Error: ", err.Error())
		return
	}
	// Now will save the questions and options
	for questionPosition := 0; questionPosition < len(request.SurveyQuestions); questionPosition++ {
		var question QuestionJSON
		question = request.SurveyQuestions[questionPosition]
		questionID, err := SaveQuestion(question.QuestionInternalID, surveyID, question.QuestionDescription, question.QuestionTip, questionPosition)
		if err != nil {
			fmt.Println("Cannot save the question! Error: ", err.Error())
			break
		}
		// Now will save the option
		for optionPosition := 0; optionPosition < len(question.QuestionOptions); optionPosition++ {
			var option OptionJSON
			option = question.QuestionOptions[optionPosition]
			_, err = SaveOption(option.OptionInternalID, questionID, option.OptionDescription, optionPosition, option.OptionBackgroundColor, option.OptionFontColor)
			if err != nil {
				fmt.Println("Cannot save the option! Error: ", err.Error())
				break
			}
		}
	}
	c.JSON(http.StatusOK, gin.H{})
}

func editSurvey(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	payload := make(map[string]string)
	payload["type"] = "crud"
	survey := LoadSurvey(id)
	render(c, gin.H{
		"title":   "Editar Pesquisa",
		"role":    getUserRole(c),
		"payload": payload,
		"survey":  survey}, "survey.html")
}

func deleteSurvey(c *gin.Context) {
	err := c.Request.ParseForm()
	checkErr(err)
	id, err := strconv.Atoi(c.Query("surveyInternalID"))
	if err == nil {
		ret := DeleteSurvey(id)
		if ret == nil {
			c.JSON(http.StatusOK, gin.H{})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{})
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{})
	}
}

func surveyList(c *gin.Context) {
	surveys := SurveyList()
	payload := make(map[string]string)
	payload["type"] = "list"
	render(c, gin.H{
		"title":   "Pesquisas",
		"payload": payload,
		"role":    getUserRole(c),
		"surveys": surveys}, "survey.html")
}

func deleteQuestion(c *gin.Context) {
	err := c.Request.ParseForm()
	checkErr(err)
	id, err := strconv.Atoi(c.Query("questionInternalID"))
	if err == nil {
		ret := DeleteQuestion(id)
		if ret == nil {
			c.JSON(http.StatusOK, gin.H{})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"error": ret.Error()})
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
}

func deleteOption(c *gin.Context) {
	err := c.Request.ParseForm()
	checkErr(err)
	id, err := strconv.Atoi(c.Query("optionInternalID"))
	if err == nil {
		ret := DeleteOption(id)
		if ret == nil {
			c.JSON(http.StatusOK, gin.H{})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"error": ret.Error()})
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
}

func showTotemMainPage(c *gin.Context) {
	id := c.DefaultQuery("id", "none")
	payload := make(map[string]string)
	if id == "none" {
		payload["type"] = "totemSelection"
		render(c, gin.H{
			"title":   "Totem",
			"payload": payload,
			"totems":  GetAvaiableTotems()}, "totem.html")
	} else {
		surveyID := GetSurvey(id)
		if surveyID != 0 {
			payload["type"] = "survey"
			render(c, gin.H{
				"title":   "Pesquisa de Satisfação",
				"payload": payload,
				"survey":  LoadSurvey(surveyID)}, "totem.html")
		} else {
			payload["type"] = "totemSelection"
			render(c, gin.H{
				"title":   "Totem",
				"payload": payload,
				"totems":  GetAvaiableTotems()}, "totem.html")
		}
	}
}
func saveAnswers(c *gin.Context) {
	var request SurveyJSON
	err := json.NewDecoder(c.Request.Body).Decode(&request)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{})
	}
	for questionPosition := 0; questionPosition < len(request.SurveyQuestions); questionPosition++ {
		var question QuestionJSON
		question = request.SurveyQuestions[questionPosition]
		questionID := question.QuestionInternalID
		for optionPosition := 0; optionPosition < len(question.QuestionOptions); optionPosition++ {
			optionID := question.QuestionOptions[optionPosition].OptionInternalID
			err = SaveAnswer(questionID, optionID)
			if err != nil {
				fmt.Println("Cannot save the answer! Error: ", err.Error())
				break
			}
		}
	}
	c.JSON(http.StatusOK, gin.H{})
}
