BEGIN TRANSACTION;
CREATE TABLE `users` (
	`username`	TEXT NOT NULL UNIQUE,
	`email`	TEXT UNIQUE,
	`name`	TEXT NOT NULL,
	`pwd`	TEXT NOT NULL,
	`admin`	BOOL DEFAULT 0,
	`created_at`	DATE,
	PRIMARY KEY(`username`)
);
INSERT INTO `users` VALUES ('bplus','bplus@bplus.com.br','BPLus Tecnologia','YnBsdXM=',1,'2017-07-10 08:36:00');

CREATE TABLE "surveys" (
	`internal_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`active`	INTEGER DEFAULT 0,
	`description`	TEXT,
	`totem`	TEXT NOT NULL,
	`initial_text`	TEXT,
	`final_text`	TEXT,
	`user_id`	TEXT NOT NULL,
	`created_at`	TIMESTAMP NOT NULL,
	`updated_at`	TIMESTAMP,
	`background_color`	TEXT DEFAULT '#fff',
	`font_color`	TEXT DEFAULT '#333',
	FOREIGN KEY(`user_id`) REFERENCES `users`
);
CREATE TABLE "questions" (
	`internal_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`description`	TEXT NOT NULL,
	`tips`	TEXT,
	`survey_id`	INTEGER NOT NULL,
	`created_at`	TIMESTAMP,
	`updated_at`	INTEGER,
	`position`	INTEGER NOT NULL,
	FOREIGN KEY(`survey_id`) REFERENCES `surveys`
);
CREATE TABLE "options" (
	`internal_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`description`	TEXT NOT NULL,
	`question_id`	INTEGER NOT NULL,
	`position`	INTEGER NOT NULL,
	`created_at`	TIMESTAMP,
	`updated_at`	INTEGER,
	`background_color`	TEXT DEFAULT '#ddd',
	`font_color`	TEXT DEFAULT '#000',
	FOREIGN KEY(`question_id`) REFERENCES `questions`
);
CREATE TABLE `answers` (
	`internal_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`question_id`	INTEGER NOT NULL,
	`option_id`	INTEGER NOT NULL,
	`created_at`	TIMESTAMP,
	FOREIGN KEY(`question_id`) REFERENCES questions,
	FOREIGN KEY(`option_id`) REFERENCES options
);
CREATE INDEX `idx_users_username_pwd` ON `users` (`username` ,`pwd` );
CREATE INDEX `idx_users_username_admin` ON `users` (`username` ,`admin` );
CREATE INDEX `idx_users_email_pwd` ON `users` (`email` ,`pwd` );
CREATE INDEX `idx_surveys_totem` ON `surveys` (`totem` );
CREATE INDEX `idx_surveys_active` ON `surveys` (`active` );
CREATE INDEX `idx_questions_survey` ON `questions` (`survey_id` );
CREATE INDEX `idx_options_question` ON `options` (`question_id` );
CREATE INDEX `idx_answers_question_option_created_at` ON `answers` (`question_id` ,`option_id` ,`created_at` );
CREATE INDEX `idx_answers_question_option` ON `answers` (`question_id` ,`option_id` );
CREATE INDEX `idx_answers_question` ON `answers` (`question_id` );
CREATE INDEX `idx_answers_option` ON `answers` (`option_id` );
CREATE INDEX `idx_answers_created_at` ON `answers` (`created_at` );
COMMIT;
