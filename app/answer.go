package main

import (
	"time"

	_ "gopkg.in/mattn/go-sqlite3.v1"
)

// SaveAnswer will save the answers
func SaveAnswer(question int, option int) error {
	createdAt := time.Now()
	stmt, err := Db.Prepare("INSERT INTO answers (option_id,created_at) VALUES (?,?,?)")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(question, option, createdAt)
	if err != nil {
		return err
	}
	return nil
}
