package main

import (
	b64 "encoding/base64"
	"math/rand"
	"net/http"
	"strconv"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gopkg.in/appleboy/gofight.v2"
)

const defaultTestingDB = "db/database.db?cache=shared&mode=rwc&_busy_timeout=5000&parseTime=true"

var debug bool
var ginMode string

func defaultTestingParams() (bool, string) {
	debug = false
	ginMode = gin.TestMode
	return debug, ginMode
}

// TestDefaultRouteRedirectToLogin if no route was especified will redirect to system login
func TestDefaultRouteRedirectToLogin(t *testing.T) {
	r := gofight.New()
	debug, ginMode := defaultTestingParams()
	r.GET("/").
		SetDebug(debug).
		Run(GinEngine(ginMode), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Equal(t, "<a href=\"/u/login\">Moved Permanently</a>.\n\n", r.Body.String())
			assert.Equal(t, http.StatusMovedPermanently, r.Code)
		})
}

// TestLoginRoute with no user logged in, this will open the login screen, and with user logged in this will redirect to home
func TestLoginRoute(t *testing.T) {
	r := gofight.New()
	debug, ginMode := defaultTestingParams()
	r.GET("/u/login").
		SetDebug(debug).
		Run(GinEngine(ginMode), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Contains(t, r.Body.String(), "<input class=\"input\" name=\"username\"")
			assert.Contains(t, r.Body.String(), "<input class=\"input\" name=\"password\"")
			assert.Equal(t, http.StatusOK, r.Code)
		})

	r.GET("/u/login").
		SetCookie(gofight.H{
			"token":      strconv.FormatInt(rand.Int63(), 16),
			"user_admin": b64.StdEncoding.EncodeToString([]byte("admin")),
		}).
		SetDebug(debug).
		Run(GinEngine(ginMode), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			assert.Contains(t, r.Body.String(), "<b>Usuário já está Logado:</b> Você já tem uma sessão ativa com o usuário!")
			assert.Equal(t, http.StatusMovedPermanently, r.Code)
		})
}

// TestLoginUnsuccessfully this will test the login with incorrect user and password, this should show the error in login screen
func TestLoginUnsuccessfully(t *testing.T) {
	r := gofight.New()
	debug, ginMode := defaultTestingParams()
	Db = initializeDB(defaultTestingDB)
	r.POST("/u/login").
		SetForm(gofight.H{
			"username": "bpl12",
			"password": "ascasd",
		}).
		SetDebug(debug).
		Run(GinEngine(ginMode), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			var err string
			err = "<b>Falha no Login:</b> Credenciais inválidas."
			assert.Contains(t, r.Body.String(), err)
			assert.Equal(t, http.StatusBadRequest, r.Code)
		})
	Db.Close()
}

// TestLoginSuccessfully this will test the login with correct user and password, this should redirect to home
func TestLoginSuccessfully(t *testing.T) {
	r := gofight.New()
	debug, ginMode := defaultTestingParams()
	Db = initializeDB(defaultTestingDB)
	r.POST("/u/login").
		SetForm(gofight.H{
			"username": "bplus",
			"password": "bplus",
		}).
		SetCookie(gofight.H{
			"token":      strconv.FormatInt(rand.Int63(), 16),
			"user_admin": b64.StdEncoding.EncodeToString([]byte("admin")),
		}).
		SetDebug(debug).
		Run(GinEngine(ginMode), func(r gofight.HTTPResponse, rq gofight.HTTPRequest) {
			var sc string
			sc = "<b>Bem Vindo!</b>"
			assert.Contains(t, r.Body.String(), sc)
			assert.Equal(t, http.StatusOK, r.Code)
		})
	Db.Close()
}
